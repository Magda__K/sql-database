# SQL database


An exemplary SQL bookstore database.
It shows the usage of structures such as:
- foreign keys
- joins
- stored functions
- subqueries
- stored procedures
- events
- extracting data using group by and having
